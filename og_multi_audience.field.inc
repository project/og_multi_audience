<?php
/**
 * @file
 * Field hook implementations for the OG Multiple Group Audience Fields module.
 */

/**
 * Implements hook_field_widget_info_alter().
 *
 * Add the membership type to the field widget form.
 */
function og_multi_audience_field_widget_info_alter(&$info) {
  $info[OG_AUDIENCE_WIDGET]['settings']['membership_type'] = OG_MEMBERSHIP_TYPE_DEFAULT;
}

/**
 * Implements hook_field_formatter_info_alter().
 *
 * Take over the field formatter so that we can show the value for group
 * audience fields other than the default group_audience that OG provides.
 */
function og_multi_audience_field_formatter_info_alter(&$info) {
  $info['og_list_default']['module'] = 'og_multi_audience';
}

/**
 * Implements hook_field_formatter_view().
 */
function og_multi_audience_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();

  // No need to show private groups several times, so remember if it was
  // already added. We don't make this static because we want to show it once
  // per field that may have a private group selected.
  $private_added = FALSE;

  // Iterate over the field's items.
  foreach ($items as $delta => $item) {
    // If we are unable to retrieve the group then we should move on to the next
    // item.
    $group = og_get_group('group', $item['gid']);
    if (!$group) {
      continue;
    }

    // If the user has access to see the group then we can display the group's
    // label
    if ($group->access()) {
      // Retrieve the group's entity so that we can get the uri.
      $entity = entity_load($group->entity_type, array($group->etid));
      $entity = current($entity);
      $uri = entity_uri($group->entity_type, $entity);

      $element[$delta] = array(
        '#type' => 'link',
        '#title' => og_label($group->gid),
        '#href' => $uri['path'],
        '#options' => array(
          'html' => TRUE,
        ),
      );
    }
    else {
      if (!$private_added) {
        $private_added = TRUE;
        $element[$delta] = array(
          '#markup' => '- ' . t('Private group') . ' -',
        );
      }
    }
  }

  return $element;
}
